package com.atlassian.confluence.extra.livesearch;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * This macro defines a live search box.
 * <p>
 * Usage:
 * <pre>
 * {livesearch} or {livesearch:spaceKey=somespacekey}
 * </pre>
 */
public class LiveSearchMacro extends BaseMacro implements Macro {
    public static final String SPACE_NAME = "space name";
    public static final String MEDIUM_SIZE = "medium";
    public static final String CONF_ALL = "conf_all";
    public static final String SELF = "@self";

    private final SpaceManager spaceManager;
    private final VelocityHelperService velocityHelperService;

    public LiveSearchMacro(SpaceManager spaceManager, VelocityHelperService velocityHelperService) {
        this.spaceManager = spaceManager;
        this.velocityHelperService = velocityHelperService;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context) {
        return TokenType.INLINE;
    }

    @Override
    public boolean hasBody() {
        return false;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    @Override
    public String execute(Map macroParams, String s, RenderContext renderContext) {
        return execute(macroParams, s, new DefaultConversionContext(renderContext));
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) {
        String spaceKey = parameters.get("spaceKey");

        String additional = parameters.get("additional");
        if (StringUtils.isBlank(additional)) {
            additional = SPACE_NAME;
        }

        String size = parameters.get("size");
        if (StringUtils.isBlank(size)) {
            size = MEDIUM_SIZE;
        }

        String labels = parameters.get("labels");
        String contentType = parameters.get("type");
        String placeholder = parameters.get("placeholder");

        final Map<String, Object> contextMap = getMacroVelocityContext();

        if (null == spaceKey) {
            contextMap.put("where", CONF_ALL);
        } else if (spaceKey.equals(SELF)) {
            contextMap.put("where", conversionContext.getSpaceKey());
        } else {
            final Space space = spaceManager.getSpace(spaceKey);
            if (space != null) contextMap.put("where", space.getKey());
        }

        contextMap.put("additional", additional);
        contextMap.put("labels", labels);
        contextMap.put("contentType", contentType);
        contextMap.put("size", size);
        contextMap.put("placeholder", placeholder);
        return renderWithVelocityTemplate(contextMap);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

    private String renderWithVelocityTemplate(Map<String, Object> contextMap) {
        return velocityHelperService.getRenderedTemplate("templates/extra/livesearch/livesearchmacro.vm", contextMap);
    }

    private Map<String, Object> getMacroVelocityContext() {
        return velocityHelperService.createDefaultVelocityContext();
    }
}