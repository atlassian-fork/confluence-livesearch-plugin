package it.com.atlassian.confluence.plugins.livesearch.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.confluence.webdriver.pageobjects.page.SearchResultPage;
import com.atlassian.confluence.webdriver.pageobjects.page.search.SearchPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.Collections.emptyList;

public class LiveSearchMacro extends ConfluenceAbstractPageComponent {

    @ElementBy(cssSelector = "form.search-macro")
    private PageElement livesearchForm;

    @ElementBy(cssSelector = "div.search-macro-query input")
    private PageElement searchbox;

    @ElementBy(cssSelector = "button.search-macro-button")
    private PageElement searchButton;

    @ElementBy(cssSelector = "div.search-macro-dropdown ol")
    private PageElement resultContent;

    public SearchPage clickSearch() {
        searchButton.click();
        return pageBinder.bind(SearchPage.class);
    }

    public String getSearchPlaceholder() {
        return searchbox.getAttribute("placeholder");
    }

    public Iterable<LiveSearchSearchResult> searchFor(String query) {
        List<PageElement> searchElements = doSearch(query);
        return searchElements.stream().map(pageElement -> {
            LiveSearchSearchResult.Builder builder = LiveSearchSearchResult.newSearchResult();

            builder.setTitle(pageElement.find(By.cssSelector("a span em")).getText())
                    .setUrl(pageElement.find(By.tagName("a")).getAttribute("href"))
                    .setContentType(pageElement.getAttribute("data-content-type"));

            if (pageElement.hasClass("with-additional"))
                builder.setAdditional(pageElement.find(By.cssSelector("a.additional")).getText());

            return builder.build();
        }).collect(Collectors.toList());
    }

    private List<PageElement> doSearch(final String query) {
        searchbox.type(query);
        driver.waitUntil(input -> {
            String lastSearch = searchbox.getAttribute("data-last-search");
            return query.equals(lastSearch);
        });

        if (isDropDownVisible()) {
            return resultContent.findAll(By.tagName("li"));
        }

        return emptyList();
    }

    public boolean isDropDownVisible() {
        return driver.elementIsVisible(By.cssSelector("div.search-macro-dropdown"));
    }

    public SearchResultPage clickSearchForMore() {
        PageElement searchFor = pageElementFinder.find(By.cssSelector("div.search-macro-dropdown .search-for"));
        waitUntilTrue(searchFor.timed().isVisible());
        searchFor.click();
        return pageBinder.bind(SearchResultPage.class);
    }
}
